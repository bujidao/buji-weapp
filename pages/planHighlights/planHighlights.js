// planHighlights.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    plans: [
      {
        title: 'Plan A',
        diff: 'this is difference',
        price: '20',
        overview: 'OVER-VIEW',
      }, {
        title: 'Plan B',
        diff: 'this is difference',
        price: '30',
        overview: 'OVER-VIEW',
      }, {
        title: 'Plan C',
        diff: 'this is difference',
        price: '60',
        overview: 'OVER-VIEW',
      }, {
        title: 'Plan D',
        diff: 'this is difference',
        price: '10',
        overview: 'OVER-VIEW',
      }, {
        title: 'Plan E',
        diff: 'this is difference',
        price: '80',
        overview: 'OVER-VIEW',
      }
    ],
    item: {
      title: 'Plan C',
      diff: 'this is difference',
      price: '60',
      overview: 'OVER-VIEW',
    }
  
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})